﻿using AgentConnect.Models.Global;
using HealthMarkets.ChatBot.Client;
using HealthMarkets.ChatBot.Models;
using HealthMarkets.ChatBot.Models.Enum;
using HealthMarkets.ChatBot.Utilities.Configuration;
using HealthMarkets.ChatBot.Utilities.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace HealthMarkets.ChatBot.Api.Controllers
{
    public class ChatBotController : ApiController
    {
        private IChatBotClient _client;

        public ChatBotController()
        {
            switch (AppSettings.DialogFlowApiVersion)
            {
                case DialogFlowApiVersion.V1:
                    _client = DialogFlowV1Client.Instance;
                    break;
                case DialogFlowApiVersion.V2:
                    _client = DialogFlowV2Client.Instance;
                    break;
            }
        }

        [HttpPost]
        public async Task<ChatBotResponse> TextRequest([FromBody] ChatBotTextRequest request)
        {
            var response = new ChatBotResponse
            {
                SessionId = request.SessionId
            };

            try
            {
                var identity = Thread.CurrentPrincipal.Identity as JwtAuthenticationIdentity;
                var chatBotId = identity.ChatBotId;
                var agentId = identity.AgentID;
                var sessionId = request.SessionId;
                var requestText = request.RequestText;

                if (request.SessionId == null)
                {
                    requestText = GetInitializationRequestText(chatBotId, agentId);
                }

                response = await _client.GetChatBotResponse(chatBotId, sessionId, requestText);

                if (response.Messages.Any())
                {
                    var firstMessage = response.Messages[0];
                    firstMessage.Audio = await TextToSpeechClient.Instance.GetAudio(chatBotId, firstMessage.GetSpeechText());
                }
            }
            catch (Exception ex)
            {

                Logging.LogException(ex, "Exception caught in ChatBotController.");
                response.Messages.Add(new ChatBotMessage("Oops... Something went wrong. Please try again later."));
            }

            if (!response.Messages.Any())
            {
                response.Messages.Add(new ChatBotMessage("I'm sorry, I didn't understand that. Please try again."));
            }

            return response;
        }

        private string GetInitializationRequestText(ChatBots chatBotId, string agentId)
        {
            // Currently ONLY Cara needs to be initialized
            if (chatBotId == ChatBots.Cara)
            {
                // Initialize
                if (string.IsNullOrEmpty(agentId))
                {
                    throw new ArgumentException("AgentId is required to initialize Cara");
                }

                return String.Format("Setup {0}", agentId);
            }

            return "hello";
        }
    }
}
