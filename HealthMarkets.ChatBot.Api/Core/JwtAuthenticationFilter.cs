﻿//using AgentConnect.Models.Global;
//using AgentConnect.Utilities.Configuration;
using AgentConnect.Models.Global;
using HealthMarkets.ChatBot.Models.Enum;
using HealthMarkets.ChatBot.Utilities.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace AgentConnect.Api.Core
{
    public class JwtAuthenticationFilter : AuthorizationFilterAttribute
    {

        public override void OnAuthorization(HttpActionContext filterContext)
        {
            if (!IsUserAuthorized(filterContext))
            {
                filterContext.Response = filterContext.Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            base.OnAuthorization(filterContext);
        }

        public bool IsUserAuthorized(HttpActionContext actionContext)
        {
            var chatBotId = FetchBotIdFromHeader(actionContext);

            if (chatBotId == ChatBots.Unknown)
                return false;

            var identity = new JwtAuthenticationIdentity("identity")
            {
                ChatBotId = chatBotId
            };

            // Only Cara needs to be authorized
            if (chatBotId != ChatBots.Cara)
            {
                Thread.CurrentPrincipal = new GenericPrincipal(identity, null);
                return true;
            }

            var authHeader = FetchFromHeader(actionContext);

            if (authHeader != null)
            {
                var handler = new JwtSecurityTokenHandler();
                var secret = AppSettings.JwtEncryptionSecret;
                var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secret));
                var tokenValidationParameters = new TokenValidationParameters()
                {
                    IssuerSigningKey = securityKey,
                    ValidateLifetime = false, // We validate lifetime below on the exp date field
                    ValidateAudience = false,
                    ValidateIssuer = false
                };

                SecurityToken validatedToken;
                try
                {
                    handler.ValidateToken(authHeader, tokenValidationParameters, out validatedToken);
                }
                catch
                {
                    return false;
                }

                if (validatedToken is JwtSecurityToken decodedJwt)
                {
                    var agentID = decodedJwt.Payload.FirstOrDefault(m => m.Key == "agentID").Value;
                    var agentIDString = agentID == null ? "" : agentID.ToString();

                    identity.AgentID = agentIDString;
                    Thread.CurrentPrincipal = new GenericPrincipal(identity, null);

                    Int64 unixTimestamp = (Int64)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
                    var exp = Convert.ToInt64(decodedJwt.Payload.FirstOrDefault(m => m.Key == "exp").Value);
                    if (exp < unixTimestamp)
                    {
                        return false;
                    }

                    return true;
                }

            }

            return false;
        }

        private string FetchFromHeader(HttpActionContext actionContext)
        {
            string requestToken = null;

            var authRequest = actionContext.Request.Headers.Authorization;
            if (authRequest != null)
            {
                requestToken = authRequest.Parameter;
            }

            return requestToken;
        }

        private ChatBots FetchBotIdFromHeader(HttpActionContext actionContext)
        {
            var headers = actionContext.Request.Headers;

            if (headers.Contains("ChatBotId"))
            {
                var chatBotIdStr = headers.GetValues("ChatBotId").First();

                return ((ChatBots)Enum.Parse(typeof(ChatBots), chatBotIdStr));
            }

            return ChatBots.Unknown;
        }
    }
}