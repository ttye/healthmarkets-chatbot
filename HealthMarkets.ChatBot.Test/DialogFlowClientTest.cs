﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HealthMarkets.ChatBot.Client;

namespace HealthMarkets.ChatBot.Test
{
    [TestClass]
    public class DialogFlowClientTest
    {
        protected DialogFlowV1Client _client;
        protected DialogFlowV2Client _clientv2;

        [TestInitialize]
        public void Init()
        {
            _client = DialogFlowV1Client.Instance;
            _clientv2 = DialogFlowV2Client.Instance;
        }

        [TestMethod]
        public void TextRequestV1Test()
        {
           // var response = _client.GetResponseMessages("Hello");
        }

        [TestMethod]
        public void TextRequestV2Test()
        {
            //var response = _clientv2.GetResponseMessagesAsync("chips");
        }
    }
}