﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public class CarouselCard
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public ChatBotLink CardImage { get; set; }
    }

    public class BrowseCarouselCard : CarouselCard
    {
        public string Footer { get; set; }
        public ChatBotLink CardLink { get; set; }
    }

    public class PlainCarouselCard : CarouselCard
    {
        public OptionInfo CardOptionInfo { get; set; }
    }

    public class OptionInfo
    {
        public string Key { get; set; }

        public List<string> Synonyms { get; set; }
    }
}
