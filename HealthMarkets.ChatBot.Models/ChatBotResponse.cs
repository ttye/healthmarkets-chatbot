﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public class ChatBotResponse
    {
        public ChatBotResponse()
        {
            Messages = new List<ChatBotMessage>();
        }

        public string SessionId { get; set; }

        public List<ChatBotMessage> Messages { get; set; }
    }
}
