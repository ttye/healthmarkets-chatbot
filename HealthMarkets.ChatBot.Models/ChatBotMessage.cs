﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public class ChatBotMessage
    {
        public ChatBotMessage()
        {

        }

        public ChatBotMessage(string text)
        {
            Type = ChatBotMessageType.Text;
            Text = text;
        }

        public ChatBotMessageType Type { get; set; }

        public string Text { get; set; }

        public object Data { get; set; }

        public string Speech { get; set; }

        public string Audio { get; set; }

        public string GetSpeechText()
        {
            return Speech ?? Text;
        }
    }
}
