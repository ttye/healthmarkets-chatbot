﻿using HealthMarkets.ChatBot.Models.Enum;

namespace HealthMarkets.ChatBot.Models
{
    public class ChatBotTextRequest
    {
        public string SessionId { get; set; }

        public string RequestText { get; set; }
    }
}
