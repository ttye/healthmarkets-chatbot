﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public class ChatBotLink
    {
        public string Text { get; set; }

        public string Url { get; set; }
    }
}
