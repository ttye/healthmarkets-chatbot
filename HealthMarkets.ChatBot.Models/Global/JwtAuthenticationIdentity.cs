﻿using HealthMarkets.ChatBot.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace AgentConnect.Models.Global
{
    public class JwtAuthenticationIdentity : GenericIdentity
    {
 
        public string AgentID { get; set; }
        public ChatBots ChatBotId  { get; set; }

        public JwtAuthenticationIdentity(string userName)
            : base(userName)
        {
        }
    }
}
