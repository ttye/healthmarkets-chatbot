﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public enum ChatBotMessageType
    {
        Text,
        Card,
        Suggestions,
        Link,
        BrowseCarousel,
        PlainCarousel,
		List
    }
}
