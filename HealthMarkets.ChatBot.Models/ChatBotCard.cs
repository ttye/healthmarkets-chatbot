﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models
{
    public class ChatBotCard
    {
        public string Title { get; set; }
        
        public string Subtitle { get; set; }

        public string Summary { get; set; }

        public string ImageSrc { get; set; }

        public ChatBotLink ButtonLink { get; set; }
    }
}
