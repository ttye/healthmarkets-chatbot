﻿using Newtonsoft.Json;

namespace HealthMarkets.ChatBot.Models
{
    public class AudioRequest
    {
        [JsonProperty("audioConfig")]
        public AudioConfig AudioConfig { get; set; }

        [JsonProperty("input")]
        public Input Input { get; set; }

        [JsonProperty("voice")]
        public Voice Voice { get; set; }
    }

    public class AudioConfig
    {
        [JsonProperty("audioEncoding")]
        public string AudioEncoding { get; set; }
        [JsonProperty("pitch")]
        public string Pitch { get; set; }
        [JsonProperty("speakingRate")]
        public string SpeakingRate { get; set; }
    }

    public class Input
    {
        //[JsonProperty("text")]
        //public string Text { get; set; }

        [JsonProperty("ssml")]
        public string SSML { get; set; }
    }

    public class Voice
    {
        [JsonProperty("languageCode")]
        public string LanguageCode { get; set; }
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}
