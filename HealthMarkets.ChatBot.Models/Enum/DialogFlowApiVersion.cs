﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models.Enum
{
    public enum DialogFlowApiVersion
    {
        V1 = 1,
        V2 = 2
    }
}
