﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Models.Enum
{
    public enum ChatBots
    {
        Unknown = 0,
        Wade = 1,
        Employees = 2,
        Cara = 3,
        Telesales = 4,
        Prospect = 5,
        SureBridge = 6,
        AgencyOps = 7,
	    Test = 99
    }
}