﻿using HealthMarkets.ChatBot.Utilities.Configuration;
using Splunk.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace HealthMarkets.ChatBot.Utilities.Logging
{
    public class Logging
    {
        private static TraceSource _traceSource;
        private static HttpEventCollectorSender ecSender;
        static Logging()
        {
            _traceSource = new TraceSource("ChatBot");
            _traceSource.Switch.Level = GetSourceLevel();
			_traceSource.Listeners.Clear();
            _traceSource.Listeners.Add(new HttpEventCollectorTraceListener(
                uri: new Uri("https://http-inputs-healthmarkets.splunkcloud.com"),
                token: AppSettings.SplunkToken));
            var middleware = new HttpEventCollectorResendMiddleware(100);
            ecSender = new HttpEventCollectorSender(new Uri("https://http-inputs-healthmarkets.splunkcloud.com"),
                AppSettings.SplunkToken,
                null,
                HttpEventCollectorSender.SendMode.Parallel,
                0,
                0,
                0,
                middleware.Plugin
            );
        }

		private static SourceLevels GetSourceLevel()
		{
			var level = AppSettings.LogLevel;
			switch (level)
			{
				case "verbose":
					return SourceLevels.Verbose;
				case "information":
					return SourceLevels.Information;
				case "warning":
					return SourceLevels.Warning;
				case "error":
					return SourceLevels.Error;
				case "critical":
					return SourceLevels.Critical;
				default:
					return SourceLevels.Off;
			}
		}

		private static void Log(int stackLevel, TraceEventType level, string message, params object[] args)
        {
            if (_traceSource != null && _traceSource.Switch.ShouldTrace(level))
            {
                string className;
                string methodName;

                GetClassAndMethodName(stackLevel + 1, out className, out methodName);
                _traceSource.TraceEvent(level, 0, FormatMessage(level, className, methodName, null, message, args));
                
            }
        }

		private static void Log(int stackLevel, TraceEventType level, Exception e, string message, params object[] args)
		{
			if (_traceSource != null && _traceSource.Switch.ShouldTrace(level))
			{
				string className;
				string methodName;

				GetClassAndMethodName(stackLevel + 1, out className, out methodName);
				_traceSource.TraceEvent(level, 0, FormatMessage(level, className, methodName, e, message, args));

			}
		}

        public static void LogBulkReassign(List<string> logs)
        {
            var msgObj = new
            {
                Logs = logs
            };

            ecSender.Send(Guid.NewGuid().ToString(), "BulkReassign", null, msgObj);
        }

        public static void LogRU(string controllerMethod, string repoMethod, string traceId, string ru)
        {
            var msgObj = new {
                ControllerMethod= controllerMethod,
                RepoMethod = repoMethod,
                TraceId = traceId,
                Ru = ru
            };
            ecSender.Send(Guid.NewGuid().ToString(), "RU", null, msgObj);
        }

        public static void LogRpmLifeEventsProcessing(Dictionary<string, int> typeCounts, Dictionary<string, string> errors, int rowsProcessed, int rowsFailed)
        {
            var msgObj = new
            {
                eventTypes = typeCounts,
                errors = errors.Select(r => new { householdId = r.Key, error = r.Value }).ToArray(),
                rowsProcessed,
                rowsFailed,
                totalRows = rowsProcessed + rowsFailed
            };
            ecSender.Send(Guid.NewGuid().ToString(), "RpmLifeEvents", null, msgObj);
        }

        public static void LogDedupe<T>(T original, T dupe)
        {
            var msgObj = new
            {
                original,
                dupe
            };

            ecSender.Send(Guid.NewGuid().ToString(), "Dedupe", null, msgObj);
        }

        /// <summary>
        /// Logs a message for the given exception
        /// </summary>
        /// <param name="stackLevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="e">Exception - Stack traces of the exception and inner exception will be included in the message (nullable) </param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogException(int stackLevel, Exception e, string message, params object[] args)
        {
            if (_traceSource != null && _traceSource.Switch.ShouldTrace(TraceEventType.Error))
            {
                var msg = ConvertExceptionToMessageObject(e);

                ecSender.Send("ChatBot", "Error", message, msg);
            }
        }

        /// <summary>
        /// Logs a message at the verbose level, calculates calling classname and methodName
        /// </summary>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogVerbose(string message, params object[] args)
        {
            LogVerbose(1, message, args);
        }

        /// <summary>
        /// Logs a message at the verbose level, calculates calling classname and methodName based on the stack level passed in
        /// </summary>
        /// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogVerbose(int stacklevel, string message, params object[] args)
        {
            Log(stacklevel + 1, TraceEventType.Verbose, message, args);
        }

        /// <summary>
        /// Logs a message at the info level, calculates calling classname and methodName
        /// </summary>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogInfo(string message, params object[] args)
        {
            LogInfo(1, message, args);
        }

        /// <summary>
        /// Logs a message at the Info level, calculates calling classname and methodName based on the passed in stack level 
        /// </summary>
        /// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogInfo(int stacklevel, string message, params object[] args)
        {
            Log(stacklevel + 1, TraceEventType.Information, message, args);
        }



        /// <summary>
        /// Logs a message at the warning level, calculates calling classname and methodName
        /// </summary>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogWarn(string message, params object[] args)
        {
            LogWarn(1, message, args);
        }

        /// <summary>
        /// Logs a message at the Warning level, calculates calling classname and methodName based on the passed in stack level
        /// </summary>
        /// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogWarn(int stacklevel, string message, params object[] args)
        {
            Log(stacklevel + 1, TraceEventType.Warning, message, args);
        }

        /// <summary>
        /// Logs an Error. Preferred Method for logging errors. Will calculate the calling classname and method name
        /// </summary>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogError(string message, params object[] args)
        {
            LogError(1, message, args);
        }

        /// <summary>
        /// Logs an Error. Preferred Method for logging errors. Will calculate the calling classname and method name
        /// based on the passed in stack level
        /// </summary>
        /// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogError(int stacklevel, string message, params object[] args)
        {
            Log(stacklevel + 1, TraceEventType.Error, message, args);
        }

		/// <summary>
		/// Logs an Error. Preferred Method for logging errors. Will calculate the calling classname and method name
		/// </summary>
		/// <param name="message">Any custom message or a format string for the passed set of arguments</param>
		/// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
		public static void LogError(Exception e, string message, params object[] args)
		{
			LogError(1, e, message, args);
		}

		/// <summary>
		/// Logs an Error. Preferred Method for logging errors. Will calculate the calling classname and method name
		/// based on the passed in stack level
		/// </summary>
		/// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
		/// <param name="message">Any custom message or a format string for the passed set of arguments</param>
		/// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
		public static void LogError(int stacklevel, Exception e, string message, params object[] args)
		{
			Log(stacklevel + 1, TraceEventType.Error, e, message, args);
		}

		/// <summary>
		/// Logs Something as Critical. Preferred Method for logging errors. Will calculate the calling classname and method name
		/// </summary>
		/// <param name="message">Any custom message or a format string for the passed set of arguments</param>
		/// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
		public static void LogCritical(string message, params object[] args)
        {
            LogCritical(1, message, args);
        }

        /// <summary>
        /// Logs Something as Critical. Preferred Method for logging errors. Will calculate the calling classname and method name
        /// based on the passed in stack level
        /// </summary>
        /// <param name="stacklevel">How far up the stack to look for the calling class and method name</param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogCritical(int stacklevel, string message, params object[] args)
        {
            Log(stacklevel + 1, TraceEventType.Critical, message, args);
        }

        /// <summary>
        /// Logs an Exception. Preferred Method for logging errors. Will calculate the calling classname and method name
        /// </summary>
        /// <param name="ex">Exception - Stack traces of the exception and inner exception will be included in the message (nullable) </param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        public static void LogException(Exception ex, string message, params object[] args)
        {
            LogException(1, ex, message, args);
        }


        /// <summary>
        /// The function looks at the stack trace and pulls out the calling class and method based on a depth
        /// </summary>
        /// <param name="stackLevel">how far up the stack to look</param>
        /// <param name="className">out parameter to hold class name</param>
        /// <param name="methodName">out parameter to hold method name</param>
        /// <returns></returns>
        private static Boolean GetClassAndMethodName(int stackLevel, out string className, out string methodName)
        {
            className = methodName = String.Empty;
            try
            {
                var callingMethod = new StackTrace(stackLevel + 1, false).GetFrame(0).GetMethod();

                methodName = callingMethod != null
                                ? callingMethod.Name
                                : "Null";

                className = callingMethod != null && callingMethod.DeclaringType != null
                                ? callingMethod.DeclaringType.FullName
                                : "Null";
            }
            catch (Exception ex)
            {
                // just log the error to diagnostics directly and continue
                _traceSource.TraceEvent(TraceEventType.Error, 0, FormatMessage(TraceEventType.Error, "Logging", "GetClassAndMethodName", ex, "Passed in StackLevel: {0}", stackLevel));
                return false;
            }
            return true;
        }

        private static List<string> ParseRelevantStackTrace(string stackTrace)
        {
            return stackTrace?.Replace("\r\n   at ", "~").Split('~').Where(stack => stack.Contains("ChatBot")).ToList();
        }

        private static List<string> ParseFullStackTrace(string stackTrace)
        {
            return stackTrace?.Replace("\r\n   at ", "~").Split('~').ToList();
        }

        /// <summary>
        /// Force a flush of the Logging to each of the listeners
        /// </summary>
        public static void FlushLog()
        {
            try
            {
                Trace.Flush();
            }
            catch (Exception ex)
            {
                // just log the error to diagnostics directly and continue
                _traceSource.TraceEvent(TraceEventType.Error, 0, FormatMessage(TraceEventType.Error, "Logging", "FlushLog", ex, String.Empty, null));
            }
        }


        /// <summary>
        ///  Formats an error message in the expected format including Exception and inner exception if available
        ///  This is called by the individual trace methods
        /// </summary>
        /// <param name="level">Any of the System.Diagnostics.TraceLevel values</param>
        /// <param name="className">name of the class that the calling method is a part of</param>
        /// <param name="methodName">the name of the method calling this function</param>
        /// <param name="e">Exception - Stack traces of the exception and inner exception will be included in the message (nullable) </param>
        /// <param name="message">Any custom message or a format string for the passed set of arguments</param>
        /// <param name="args"> these are values to be used when the message is a Format String (nullable)</param>
        /// <returns>String including messages and Stack Traces</returns>
        private static string FormatMessage(TraceEventType level, string className, string methodName,
                Exception e, string message, params object[] args)
        {
            // Use the passed message as the initializer for our error string
            StringBuilder messageTrace = new StringBuilder(args == null ? message : String.Format(message, args));
            // if an error was passed, interrogate it
            string exceptionString = GetMessageAndStackTraceRecursively(e, messageTrace);

            // Format the String in the manner we want it to show up in the media 
            return String.Format("{0}\t{1}\t{2}\t{3} {4}\t{5}\t{6}"
                , "LoggingChannel=ChatBot"
                , DateTime.UtcNow.ToString(@"yyyy-MM-dd HH:mm:ss,fff")
                , level
                , className
                , Thread.CurrentThread.ManagedThreadId
                , methodName + ' ' + messageTrace
                , exceptionString
                );
        }

        /// <summary>
        /// Constructs an error message based on current exception and all inner exceptions recursively
        /// </summary>
        /// <param name="ex">The exception to interrogate</param>
        /// <returns>an empty string in the case of null Exception or concatination
        /// of the exception and all inner exceptions including stack traces</returns>
        private static string GetMessageAndStackTraceRecursively(Exception ex, StringBuilder messageTrace)
        {
            if (ex != null)
            {

                // If there are entries in ex.Data, create an additional 'Data' parameter to log,
                // along with the associated key/value pairs
                if (ex.Data.Count > 0)
                {
                    var dataCollectionBuilder = new StringBuilder();
                    // Ensure there is a leading space
                    dataCollectionBuilder.Append(" Data=");
                    foreach (DictionaryEntry datum in ex.Data)
                    {
                        // Example:
                        // 'proposalId'='c78a6409-1e79-4ad5-9926-3032041949fb'
                        // 'comments':'This was likely the result of a data error'
                        dataCollectionBuilder.AppendFormat("{0}= {1}, ", datum.Key, datum.Value);
                    }
                    messageTrace.Append(dataCollectionBuilder.ToString());
                }

                var result = String.Format(" Exception: Type={0} Message={1} StackTrace={2} InnerException={3}"
                                           , ex.GetType()
                                           , ex.Message
                                           , ex.StackTrace
                                           ,
                                           ex.InnerException != null
                                               ? GetMessageAndStackTraceRecursively(ex.InnerException, messageTrace)
                                               : String.Empty);

                return result;
            }

            return String.Empty;
        }

        private static object ConvertExceptionToMessageObject(Exception ex)
        {
            if (ex == null)
                return null;

            var msgObj = new
            {
                InnerException = ConvertExceptionToMessageObject(ex.InnerException),
                ACStackTrace = ParseRelevantStackTrace(ex.StackTrace),
                FullStackTrace = ParseFullStackTrace(ex.StackTrace),
                Message = ex.Message,
                Data = ex.Data
            };

            return msgObj;
                 
        }

    }
}
