﻿using HealthMarkets.ChatBot.Models.Enum;
using System;
using System.Configuration;

namespace HealthMarkets.ChatBot.Utilities.Configuration
{
    public class AppSettings
    {
        public static DialogFlowApiVersion DialogFlowApiVersion
        {
            get
            {
                var str = ConfigurationManager.AppSettings["DialogFlowApiVersion"];
                return (DialogFlowApiVersion)Enum.Parse(typeof(DialogFlowApiVersion), str);
            }
        }
        public static string JwtEncryptionSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["jwtEncryptionSecret"];
            }
        }

        public static string TextToSpeechUri
        {
            get
            {
                return ConfigurationManager.AppSettings["TextToSpeechUri"];
            }
        }

        public static string TextToSpeechKey
        {
            get
            {
                return ConfigurationManager.AppSettings["TextToSpeechKey"];
            }
        }

        public static string SplunkToken
        {
            get
            {
                return ConfigurationManager.AppSettings["SplunkToken"];
            }
        }

        public static string LogLevel
        {
            get
            {
                return ConfigurationManager.AppSettings["LogLevel"];
            }
        }

        public static string GetChatBotAgentProjectId(ChatBots chatBotId)
        {
            var str = chatBotId.ToString() + "AgentProjectId";
            return ConfigurationManager.AppSettings[str];
        }

        public static string GetChatBotAgentApiKey(ChatBots chatBotId)
        {
            var str = chatBotId.ToString() + "AgentApiKey";
            return ConfigurationManager.AppSettings[str];
        }

        public static string GetChatBotVoiceCode(ChatBots chatBotId)
        {
            var str = chatBotId.ToString() + "VoiceCode";
            return ConfigurationManager.AppSettings[str];
        }
    }
}
