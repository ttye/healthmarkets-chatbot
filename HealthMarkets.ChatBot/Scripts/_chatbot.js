function setBot(botName) {
    window.location = "?chatbot=" + botName;
}

$(document).ready(function () {
    botElement = $("chatBot").first();
    company = botElement.attr("company");
    botName = window.location.href.split('?chatbot=')[1] || botElement.attr("bot");
    chatIconSrc = botElement.attr("chatIconSrc");
    botImageSrc = botElement.attr("botImageSrc");

    // Enums
    chatBots = Object.freeze({ Wade: 1, Employees: 2, Cara: 3, Telesales: 4, Prospect: 5, SureBridge: 6, AgencyOps: 7, Test: 99 });
    companies = Object.freeze({ HealthMarkets: 'healthmarkets', Excelsior: 'excelsior', SureBridge: 'surebridge', Benefiter: 'benefiter' });

    // Set Brand
    cssClass = companies[company];
    botElement.addClass(cssClass);

    chatBotId = chatBots[botName];

    browserType = navigator.userAgent;
    isMobile = (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(browserType));

    $("chatBot").append(
        '<input id = \"open-chat-bot\" type=\"image\" src=\"' + chatIconSrc + '" />' +
        '<audio id="fakePlayer"></audio>' +
        '<div class=\"chat-box\" >' +
        '<div class=\"chat-header\">' +
            '<i id="close-chat-bot" class="fa fa-times"></i>' +
            '<div class="chat-header-content">' +
                '<div class="chat-bot-image-div">' +
                    '<img class="chatbot-image" src="' + botImageSrc + '" alt="ChatBot">' +
                '</div>' +
                    '<div class="chat-bot-name">' +
                    '<label class=\"chat-label-2line\"><b>I\'m ' + botName + ' the  </b></br>' + '<b>' + company + ' ChatBot</b></label>' +
                    '<label class=\"chat-label-1line\"><b>I\'m ' + botName + ' the ' + company + ' ChatBot</b></label>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="botui-app-container" id="healthMarketsBot">' +
                '<bot-ui></bot-ui>' +
            '</div>' +
            '<div class="userEntry">' +
                '<form id="userEntryForm">' +
                    '<textarea disabled maxlength="150" class="userEntryText" id="userEntryText" type="text" placeholder="Enter message here"></textarea>' +
                '</form>' +
            '</div>' +
        '</div>');


    botui = new BotUI('healthMarketsBot');

    $("#userEntryText").keypress(function (e) {
        if (e.which == 13 && !e.shiftKey) {
            if (typeof snd !== 'undefined') {
                snd.pause();
            }

            e.preventDefault();    //stop form from submitting

            var message = $("#userEntryText")[0].value;

            if (message !== undefined || message !== '') {
                $("#userEntryText")[0].value = '';

                //on mobile close the input
                if (isMobile) {
                    $("#userEntryText").blur();
                }
                addHumanMessage(message);
                getChatBotResponse(message);
            }
        }
    });

    $("#open-chat-bot, #close-chat-bot").click(function () {
        /*snd = document.getElementById('fakePlayer');
        snd.play().then(_ => {
            snd.pause();
        }); // Play the empty element to get control*/

        $(".chat-box").toggle(500);
        $("#open-chat-bot").toggle(500);

        if ($(".botui-message").length == 0) {
            init();
        }
    });

    lastHeight = $(window).height(); 

    var isAndroid = browserType.indexOf("Android") > -1; //&& ua.indexOf("mobile");
    if (isAndroid) {
        $(window).resize(function () {
            orientationChange();
        });
    }

});



var orientationChange = function () {
    var $element = $('.chat-box');
    //$element.css('height', '100vh');
    $element.css('height', lastHeight + 'px');

    var $entryDiv = $("#userEntryForm");
    var $entryText = $(".userEntryText");

    var visibleHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
    var keyboardHeight = $('.chat-box').height() - visibleHeight;
    var entryHeight = keyboardHeight + 45;

    if (lastHeight > $(window).height()) {
        $entryDiv.css('height', entryHeight + 'px');
        $entryText.css('height', '45px');
        //$entryDiv.css('height', $element.height() + 'px');
    } else {
        $entryDiv.css('height', '7%');
        //$entryDiv.css('height', $element.height() + 'px');
    }
};

var removeAudioButton = function () {

    $("#AudioButton").remove();
    $(".click-for-speech").css('cursor', 'auto');
};

var sessionId;
var isDemo = true;

function init() {

    if (chatBotConfig.auth && chatBotId === chatBots.Cara) {

        $.ajax({
            url: chatBotConfig.auth.url,
            type: "POST",
            headers: {
                username: chatBotConfig.auth.username,
                password: chatBotConfig.auth.password,
                'Content-Type': 'application/json; charset=utf-8',
                Accept: 'application/json',
            },
            dataType: "json",
            success: function (data) {
                localStorage.setItem('jwt', data.jwt);
                getChatBotResponse('hello');
            },
            error: function (textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });

    } else {
        getChatBotResponse('hello');
    }
}

var addHumanMessage = function (message) {

    botui.action.hide(); // hide actions if there are any

    botui.message.add({
        human: true, // show it as right aligned to UI
        content: message
    }).then(function () {
        scrollChatContainer();
    });


}

var speak = function (audioContent) {
    snd = new Audio("data:audio/wav;base64," + audioContent);

    if (isMobile) {
        $("#AudioButton").show();
        $(".click-for-speech").click(function () {
            snd.play();
        });
    } else {
        snd.play();
    }
}

var msgTypeEnum = Object.freeze({ Text: 0, Card: 1, SuggestionChips: 2, Link: 3, BrowseCarousel: 4, PlainCarousel: 5, List: 6 });

var processMessages = function (messages, firstMessageIndex) {

    var firstResponse = messages[0];

    if (firstResponse.Type === msgTypeEnum.Text) {
        removeAudioButton();

        var firstResponseText = firstResponse.Text;
        console.log('Success:', firstResponseText);

        botui.message.update(firstMessageIndex, {
            loading: false,
            content: firstResponseText
        }).then(function (i) {
            speak(firstResponse.Audio);
            messages.shift();
            renderMessages(messages);

            $("#userEntryText")[0].disabled = false;

            scrollChatContainer();
        });
    } else {
        botui.message.remove(firstMessageIndex);
        renderMessages(messages);
        scrollChatContainer();
    }
}

var renderMessages = function (response) {

    response.forEach(function (msg) {

        var messageForBot;

        if (msg.Type === msgTypeEnum.Text) {
            removeAudioButton();     
            messageForBot = msg.Text;

        } else if (msg.Type === msgTypeEnum.Card) {

            messageForBot = {
                type: 'card',
                content: msg.Data
            };

        } else if (msg.Type === msgTypeEnum.SuggestionChips) {

            var suggestions = [];

            msg.Data.forEach(function (suggestion) {
                var suggestionButton = {
                    text: suggestion,
                    value: suggestion
                };
                suggestions.push(suggestionButton);
            });

            botui.action.button({
                delay: 300,
                action: suggestions
            }).then(function (res) {
                getChatBotResponse(res.value);
            });

            scrollChatContainer();

        } else if (msg.Type === msgTypeEnum.Link) {

            messageForBot = {
                type: 'link',
                content: {
                    text: msg.Data.Text,
                    url: msg.Data.Url
                }
            };

        } else if (msg.Type === msgTypeEnum.BrowseCarousel) {

            var carouselCards = [];

            msg.Data.forEach(function (carouselCard) {
                {
                    carouselCards.push({
                        image_src: carouselCard.CardImage.Url,
                        title: carouselCard.Title,
                        description: carouselCard.Description,
                        image_text: carouselCard.CardImage.Text,
                        footer: carouselCard.Footer,
                        url: carouselCard.CardLink.Url,
                        url_type_hint: carouselCard.CardLink.urlTypeHint
                    })
                }
            });

            messageForBot = {
                type: 'browseCarousel',
                content: carouselCards
            };

        } else if (msg.Type === msgTypeEnum.PlainCarousel) {

            var plainCarouselCards = [];

            msg.Data.forEach(function (carouselCard) {
                {
                    plainCarouselCards.push({
                        image_src: carouselCard.CardImage.Url,
                        title: carouselCard.Title,
                        description: carouselCard.Description,
                        image_text: carouselCard.CardImage.Text
                    })
                }
            });

            messageForBot = {
                type: 'plainCarousel',
                content: plainCarouselCards
            };
        } else if (msg.Type === msgTypeEnum.List) {
            botui.action.list({
                delay: 300,
                action: msg.Data
            }).then(function (res) {
                getChatBotResponse(res.Id);;
                });

            scrollChatContainer();
        }

        if (messageForBot) {
            botui.message.add(messageForBot).then(function (i) {

                if ($('#bcarousel-' + i)) {
                    renderCarousel(i);
                    removeAudioButton();
                }
            });
        }
    });
};

var scrollChatContainer = function () {

    setTimeout(function () {

        var scrollDiv = $(".simplebar-scroll-content");
        var scrollHeight = scrollDiv[0].scrollHeight;

        scrollDiv.animate({
            scrollTop: scrollHeight
            }, "slow");
        
    }, 500);

}
var renderCarousel = function (i) {
    var carouselID = "bcarousel-" + i;
    console.log("rendering carousel: " + carouselID);

    $('#' + carouselID).slick({
        autoplay: false,
        arrows: true,
        nextArrow: '<img class="next" src="https://image.flaticon.com/icons/svg/137/137513.svg">',
        prevArrow: '<img class="pre" src="https://image.flaticon.com/icons/svg/137/137514.svg">',
        dots: false,
        centerMode: true,
        centerpadding: '60',
        variableWidth: true,
        focusOnSelect: true,
        slidesPerRow: 1
    });

}
var getChatBotResponse = function (message) {

    var request = {
        ChatBotId: chatBotId,
        SessionId: sessionId,
        RequestText: message
    };

    botui.message.add({
        loading: true // show loading dots while waiting
    }).then(function (index) {

        var url = chatBotConfig.api.host + chatBotConfig.api.textRequestPath;

        $.ajax({
            url: url,
            type: "POST",
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localStorage.getItem('jwt'),
                'ChatBotId': chatBotId
            },
            data: JSON.stringify(request),
            dataType: "json",
            success: function (data) {
                if (data.SessionId) {
                    sessionId = data.SessionId; // Save SessionId
                }
                processMessages(data.Messages, index);
            },
            error: function (textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    })
}