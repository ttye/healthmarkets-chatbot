﻿module.exports = function (grunt) {

    grunt.initConfig({
        copy: {
            debugConfig: {
                src: 'config/debug.js',
                dest: 'config/config.js'
            },
            devConfig: {
                src: 'config/dev.js',
                dest: 'config/config.js'
            },
            suppConfig: {
                src: 'config/supp.js',
                dest: 'config/config.js'
            },
            releaseConfig: {
                src: 'config/release.js',
                dest: 'config/config.js'
            }
        },
        concat: {
            js: {
                src: ['Scripts/BotUI.js', 'Scripts/jquery-3.3.1.min.js', 'Scripts/vue.min.js', 'Config/config.js', 'Scripts/_chatbot.js', 'Scripts/slick.min.js', 'Scripts/simplebar.js'],
                dest: 'chatbot.js',
            },
            css: {
                src: ['Styles/botui.css', 'Styles/botui-theme-default.css', 'Styles/DemoPage.css', 'Styles/slick-theme.css', 'Styles/slick.css', 'Styles/open-sans.css', 'Styles/_chatbot.css', 'Styles/brands.css'],
                dest: 'chatbot.css',
            }
        },
        watch: {
            scripts: {
                files: ['**/*.js'],
                tasks: ['concat:js'],
                options: {
                    spawn: false,
                }
            },
            css: {
                files: ['**/*.css'],
                tasks: ['concat:css'],
                options: {
                    spawn: false,
                }
            }
        },
        uglify: {
            options: {
                mangle: false
            },
            js: {
                files: {
                    'chatbot.min.js': ['chatbot.js']
                }
            }
        },
        cssmin: {
            css: {
                files: {
                    'chatbot.min.css': ['chatbot.css']
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-uglify-es');
    grunt.loadNpmTasks('grunt-contrib-cssmin');

    grunt.registerTask('build-Debug', ['copy:debugConfig', 'concat']);
    grunt.registerTask('build-Dev', ['copy:devConfig', 'concat', 'uglify', 'cssmin']);
    grunt.registerTask('build-Supp', ['copy:suppConfig', 'concat', 'uglify', 'cssmin']);
    grunt.registerTask('build-Release', ['copy:releaseConfig', 'concat', 'uglify', 'cssmin']);
};