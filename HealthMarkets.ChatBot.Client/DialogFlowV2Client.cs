﻿using Google.Cloud.Dialogflow.V2;
using HealthMarkets.ChatBot.Models;
using HealthMarkets.ChatBot.Models.Enum;
using HealthMarkets.ChatBot.Utilities.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Google.Cloud.Dialogflow.V2.Intent.Types.Message;
using static Google.Cloud.Dialogflow.V2.Intent.Types.Message.Types;

namespace HealthMarkets.ChatBot.Client
{
    public class DialogFlowV2Client : IChatBotClient
    {
        public static DialogFlowV2Client Instance
        {
            get
            {
                return new DialogFlowV2Client();
            }
        }

        public DialogFlowV2Client()
        {

        }

        public async Task<ChatBotResponse> GetChatBotResponse(ChatBots chatBotId, string sessionId, string requestText)
        {
            var projectId = AppSettings.GetChatBotAgentProjectId(chatBotId);
            sessionId = sessionId ?? Guid.NewGuid().ToString();

            // NOTE: You need to set the GOOGLE_APPLICATION_CREDENTIALS enviroment variable 
            // to your Google Service Account credentials json file.

            // To Verify your credentials, if you recently changed your env variable, you will need to restart VS
            //var credential = GoogleCredential.GetApplicationDefault();

            var client = SessionsClient.Create();

            var response = await client.DetectIntentAsync(
                session: new SessionName(projectId, sessionId),
                queryInput: new QueryInput
                {
                    Text = new TextInput
                    {
                        Text = requestText,
                        LanguageCode = "en-US"
                    }
                }
            );

            var defaultText = response.QueryResult.FulfillmentText;
            // TODO: Test SSML responses in wade, figure out where speech comes from
            var speech = response.QueryResult.FulfillmentText;
            var messages = response.QueryResult.FulfillmentMessages.ToList();

            var chatBotResponse = new ChatBotResponse
            {
                SessionId = sessionId,
                Messages = Process(messages, defaultText, speech)
            };

            return chatBotResponse;
        }

        private List<ChatBotMessage> Process(List<Intent.Types.Message> responseMessages, string defaultText, string speech)
        {
            var messages = new List<ChatBotMessage>();
            var addSimpleResponse = false;

            if (!string.IsNullOrEmpty(defaultText))
            {
                var message = new ChatBotMessage(defaultText)
                {
                    Speech = speech
                };

                messages.Add(message);
            }
            else
            {
                addSimpleResponse = true;
            }
            
            foreach (var responseMessage in responseMessages)
            {
                ChatBotMessage message = null;

                if (responseMessage.Platform != Platform.ActionsOnGoogle)
                {
                    continue;
                }

                switch (responseMessage.MessageCase)
                {
                    case MessageOneofCase.SimpleResponses:
                        {
                            if (addSimpleResponse)
                            {
                                foreach (var response in responseMessage.SimpleResponses.SimpleResponses_)
                                {
                                    messages.Add(new ChatBotMessage
                                    {
                                        Type = ChatBotMessageType.Text,
                                        Text = response.DisplayText ?? response.TextToSpeech,
                                        Speech = response.TextToSpeech
                                    });
                                }
                            }
                            break;
                        }
                    case MessageOneofCase.BasicCard:
                        {
                            ChatBotLink buttons = null;

                            if (responseMessage.BasicCard.Buttons.Any())
                            {
                                buttons = new ChatBotLink
                                {
                                    Text = responseMessage.BasicCard.Buttons[0].Title,
                                    Url = responseMessage.BasicCard.Buttons[0].OpenUriAction.Uri
                                };
                            }

                            message = new ChatBotMessage
                            {
                                Type = ChatBotMessageType.Card,
                                Data = new ChatBotCard
                                {
                                    Title = responseMessage.BasicCard.Title,
                                    Subtitle = responseMessage.BasicCard.Subtitle,
                                    Summary = responseMessage.BasicCard.FormattedText,
                                    ImageSrc = responseMessage.BasicCard.Image.ImageUri,
                                    ButtonLink = buttons
                                }
                            };
                            break;
                        }
                    case MessageOneofCase.Suggestions:
                        {
                            var suggestions = new List<string>();
                            foreach (var suggestion in responseMessage.Suggestions.Suggestions_)
                            {
                                suggestions.Add(suggestion.Title);
                            }

                            if (suggestions.Any())
                            {
                                message = new ChatBotMessage
                                {
                                    Type = ChatBotMessageType.Suggestions,
                                    Data = suggestions
                                };
                            }
                            break;
                        }
                    case MessageOneofCase.LinkOutSuggestion:
                        {
                            message = new ChatBotMessage
                            {
                                Type = ChatBotMessageType.Link,
                                Data = new ChatBotLink
                                {
                                    Text = responseMessage.LinkOutSuggestion.DestinationName,
                                    Url = responseMessage.LinkOutSuggestion.Uri
                                }
                            };
                            break;
                        }
                    case MessageOneofCase.CarouselSelect:
                        {
                            var cards = new List<PlainCarouselCard>();
                            foreach (var card in responseMessage.CarouselSelect.Items)
                            {
                                cards.Add(new PlainCarouselCard()
                                {
                                    Title = card.Title,
                                    Description = card.Description,
                                    CardImage = new ChatBotLink
                                    {
                                        Text = card.Image == null ? "" : card.Image.AccessibilityText,
                                        Url = card.Image == null ? "" : card.Image.ImageUri
                                    },
                                    CardOptionInfo = new OptionInfo
                                    {
                                        Key = card.Info == null ? "" : card.Info.Key,
                                        Synonyms = card.Info == null ? new List<string>() : card.Info.Synonyms.ToList()
                                    }
                                });
                            }

                            if (cards.Any())
                            {
                                message = new ChatBotMessage
                                {
                                    Type = ChatBotMessageType.PlainCarousel,
                                    Data = cards
                                };
                            }

                            break;
                        }
                    //TODO: Google.Cloud.Dialogflow.V2 does not currently support Browse Carousel....
                    //case MessageOneofCase.BrowseCarousel?????
                    //    {
                    //        var cards = new List<BrowseCarouselCard>();
                    //        foreach (var card in responseMessage.BrowseCarousel.Items)
                    //        {
                    //            cards.Add(new BrowseCarouselCard()
                    //            {
                    //                Footer = card.footer,
                    //                CardLink = new ChatBotLink
                    //                {
                    //                    Text = card.openUrlAction == null ? "" : card.openUrlAction.urlTypeHint,
                    //                    Url = card.openUrlAction == null ? "" : card.openUrlAction.url
                    //                },
                    //                Title = card.Title,
                    //                Description = card.Description,
                    //                CardImage = new ChatBotLink
                    //                {
                    //                    Text = card.Image == null ? "" : card.Image.AccessibilityText,
                    //                    Url = card.Image == null ? "" : card.Image.ImageUri
                    //                }
                    //            });
                    //        }

                    //        if (cards.Any())
                    //        {
                    //            message = new ChatBotMessage
                    //            {
                    //                Type = ChatBotMessageType.BrowseCarousel,
                    //                Data = cards
                    //            };
                    //        }
                    //    }
                    case MessageOneofCase.ListSelect:
                        {
                            var items = new List<ChatBotListItem>();
                            foreach (var item in responseMessage.ListSelect.Items)
                            {
                                items.Add(new ChatBotListItem
                                {
                                    Id = item.Info?.Key,
                                    Title = item.Title,
                                    Description = item.Description,
                                    ImageSrc = item.Image?.ImageUri
                                });
                            }
                            message = new ChatBotMessage
                            {
                                Type = ChatBotMessageType.List,
                                Data = new ChatBotList
                                {
                                    Title = responseMessage.ListSelect.Title,
                                    Items = items
                                }
                            };

                            break;
                        }
                }

                if (message != null)
                {
                    messages.Add(message);
                }
            }

            return messages;
        }
    }
}
