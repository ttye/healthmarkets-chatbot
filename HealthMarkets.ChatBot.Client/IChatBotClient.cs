﻿using HealthMarkets.ChatBot.Models;
using HealthMarkets.ChatBot.Models.Enum;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Client
{
    public interface IChatBotClient
    {
        Task<ChatBotResponse> GetChatBotResponse(ChatBots chatBotId, string sessionId, string requestText);
    }
}
