﻿using ApiAiSDK;
using HealthMarkets.ChatBot.Models;
using HealthMarkets.ChatBot.Models.Enum;
using HealthMarkets.ChatBot.Utilities.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Client
{
    public class DialogFlowV1Client : IChatBotClient
    {
        public static DialogFlowV1Client Instance
        {
            get
            {
                return new DialogFlowV1Client();
            }
        }

        public async Task<ChatBotResponse> GetChatBotResponse(ChatBots chatBotId, string sessionId, string requestText)
        {
            // Initialize
            var agentKey = AppSettings.GetChatBotAgentApiKey(chatBotId);

            var config = new AIConfiguration(agentKey, SupportedLanguage.English)
            {
                SessionId = sessionId
            };

            var _client = new ApiAi(config);

            // Request
            var response = _client.TextRequest(requestText);

            // Process Response
            var defaultText = response.Result.Fulfillment.DisplayText;
            var speech = response.Result.Fulfillment.Speech;
            var messages = response.Result.Fulfillment.Messages;

            var chatBotResponse = new ChatBotResponse
            {
                SessionId = response.SessionId,
                Messages = Process(messages, defaultText, speech)
            };

            return chatBotResponse;
        }

        private List<ChatBotMessage> Process(List<object> responseMessages, string defaultText, string speech)
        {
            var messages = new List<ChatBotMessage>();
            var addSimpleResponse = false;

            if (!string.IsNullOrEmpty(defaultText) || !string.IsNullOrEmpty(speech))
            {
                var displyText = defaultText ?? speech;
                var message = new ChatBotMessage(displyText)
                {
                    Speech = speech
                };

                messages.Add(message);
            }
            else
            {
                addSimpleResponse = true;
            }

            if (responseMessages == null || !responseMessages.Any())
            {
                return messages;
            }

            foreach (var responseMessage in responseMessages)
            {
                var message = ProcessMessage(responseMessage, addSimpleResponse);

                if (message != null)
                {
                    messages.Add(message);
                }
            }

            return messages;
        }


        private ChatBotMessage ProcessMessage(dynamic responseMessage, bool addSimpleResponse)
        {
            ChatBotMessage message = null;

            if (responseMessage.platform != "google")
            {
                return null;
            }

            if (addSimpleResponse && responseMessage.type == "simple_response")
            {
                message = new ChatBotMessage
                {
                    Type = ChatBotMessageType.Text,
                    Text = responseMessage.textToSpeech
                };
            }
            else if (responseMessage.type == "basic_card")
            {
                ChatBotLink buttons = null;

                if (responseMessage.buttons != null)
                {
                    var firstButton = responseMessage.buttons[0];
                    buttons = new ChatBotLink
                    {
                        Text = firstButton.title,
                        Url = firstButton.openUrlAction != null ? firstButton.openUrlAction.url : null
                    };
                }

                message = new ChatBotMessage
                {
                    Type = ChatBotMessageType.Card,
                    Data = new ChatBotCard
                    {
                        Title = responseMessage.title,
                        Subtitle = responseMessage.subtitle,
                        Summary = responseMessage.formattedText,
                        ImageSrc = responseMessage.image != null ? responseMessage.image.url : null,
                        ButtonLink = buttons
                    }
                };
            }
            else if (responseMessage.type == "suggestion_chips")
            {
                var suggestions = new List<string>();
                foreach (var suggestion in responseMessage.suggestions)
                {
                    suggestions.Add((string)suggestion.title);
                }

                if (suggestions.Any())
                {
                    message = new ChatBotMessage
                    {
                        Type = ChatBotMessageType.Suggestions,
                        Data = suggestions
                    };
                }
            }
            else if (responseMessage.type == "link_out_chip")
            {
                message = new ChatBotMessage
                {
                    Type = ChatBotMessageType.Link,
                    Data = new ChatBotLink
                    {
                        Text = responseMessage.destinationName,
                        Url = responseMessage.url
                    }
                };
            }
            else if (responseMessage.type == "carousel_card")
            {
                var cards = new List<PlainCarouselCard>();
                foreach (var card in responseMessage.items)
                {
                    cards.Add(new PlainCarouselCard()
                    {
                        Title = card.title,
                        Description = card.description,
                        CardImage = new ChatBotLink
                        {
                            Text = card.image == null ? "" : card.image.accessibilityText,
                            Url = card.image == null ? "" : card.image.url
                        },
                        CardOptionInfo = new OptionInfo
                        {
                            Key = card.optionInfo == null ? "" : card.optionInfo.key,
                            Synonyms = card.optionInfo == null ? new List<string>() : card.optionInfo.synonyms.ToObject<List<string>>()
                        }
                    });
                }

                if (cards.Any())
                {
                    message = new ChatBotMessage
                    {
                        Type = ChatBotMessageType.PlainCarousel,
                        Data = cards
                    };
                }
            }
            else if (responseMessage.type == "browse_carousel_card")
            {
                var cards = new List<BrowseCarouselCard>();
                foreach (var card in responseMessage.items)
                {
                    cards.Add(new BrowseCarouselCard()
                    {
                        Footer = card.footer,
                        CardLink = new ChatBotLink
                        {
                            Text = card.openUrlAction == null ? "" : card.openUrlAction.urlTypeHint,
                            Url = card.openUrlAction == null ? "" : card.openUrlAction.url
                        },
                        Title = card.title,
                        Description = card.description,
                        CardImage = new ChatBotLink
                        {
                            Text = card.image == null ? "" : card.image.accessibilityText,
                            Url = card.image == null ? "" : card.image.url
                        }
                    });
                }

                if (cards.Any())
                {
                    message = new ChatBotMessage
                    {
                        Type = ChatBotMessageType.BrowseCarousel,
                        Data = cards
                    };
                }
            }
            else if (responseMessage.type == "list_card")
            {
                var items = new List<ChatBotListItem>();
                foreach (var item in responseMessage.items)
                {
                    items.Add(new ChatBotListItem
                    {
                        Id = item.optionInfo != null ? item.optionInfo.key : null,
                        Title = item.title,
                        Description = item.description,
                        ImageSrc = item.image != null ? item.image.url : null
                    });
                }
                message = new ChatBotMessage
                {
                    Type = ChatBotMessageType.List,
                    Data = new ChatBotList
                    {
                        Title = responseMessage.title,
                        Items = items
                    }
                };
            }

            return message;
        }
    }
}

public class ChatBotList
{
    public string Title { get; set; }
    
    public List<ChatBotListItem> Items { get; set; }
}

public class ChatBotListItem
{
    public string Id { get; set; }

    public string ImageSrc { get; set; }

    public string Title { get; set; }

    public string Description { get; set; }
}