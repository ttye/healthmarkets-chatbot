﻿using HealthMarkets.ChatBot.Models;
using HealthMarkets.ChatBot.Models.Enum;
using HealthMarkets.ChatBot.Utilities.Configuration;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace HealthMarkets.ChatBot.Client
{
    public class TextToSpeechClient
    {
        public static TextToSpeechClient Instance
        {
            get
            {
                return new TextToSpeechClient();
            }
        }
        public async Task<string> GetAudio(ChatBots chatBotId, string speechText)
        {

            if (string.IsNullOrEmpty(speechText))
            {
                return null;
            }

            var audioRequest = new AudioRequest
            {
                AudioConfig = new AudioConfig
                {
                    AudioEncoding = "MP3",
                    Pitch = "0.00",
                    SpeakingRate = "1.00"
                },
                Input = new Input
                {
                    //Text = text
                    SSML = speechText
                },
                Voice = new Voice
                {
                    LanguageCode = "en-US",
                    Name = AppSettings.GetChatBotVoiceCode(chatBotId)
                }
            };

            using (var client = new HttpClient())
            {
                var audioURL = string.Format("{0}?key={1}", AppSettings.TextToSpeechUri, AppSettings.TextToSpeechKey);

                var audioJsonString = JsonConvert.SerializeObject(audioRequest);
                var content = new StringContent(audioJsonString, Encoding.UTF8, "application/json");

                HttpResponseMessage response =  await client.PostAsync(audioURL, content);
                string audioResponse = await response.Content.ReadAsStringAsync();
                dynamic audioJson = JsonConvert.DeserializeObject(audioResponse);

                return audioJson.audioContent;
            }
        }
    }
}
